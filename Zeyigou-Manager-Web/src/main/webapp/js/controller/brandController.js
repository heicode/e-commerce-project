app.controller("brandController",function($scope,$controller,brandService){
    $controller('baseController',{$scope:$scope});//继承
    //7.3）查询品牌列表
    $scope.findAll=()=>{
        brandService.success(response=>{
            $scope.brandList = response;
        })
    }

    //7.4)分页查询
    $scope.findByPage=()=>{
        brandService.findByPage($scope.paginationConf.currentPage,$scope.paginationConf.itemsPerPage)
            .success(response=>{		//返回的是一个pageResult
                //7.4.1)更新总记录数
                $scope.paginationConf.totalItems = response.total;
                //7.4.2)为当前页集合设置值
                $scope.brandList = response.rows;
            })
    }
    //7.5)分页带条件查询
    $scope.search=()=>{
        brandService.search($scope.paginationConf.currentPage,$scope.paginationConf.itemsPerPage,$scope.searchEntity)
            .success(response=>{		//返回的是一个pageResult
                //7.5.1)更新总记录数
                $scope.paginationConf.totalItems = response.total;
                //7.5.2)为当前页集合设置值
                $scope.brandList = response.rows;
            })
    }

    //7.6)修改界面
    $scope.updateUI=(brand)=>{
        $scope.entity = brand;
    }

    //7.7)保存品牌(有id就是修改，否则，添加)
    $scope.save=()=>{
        //7.7.1)定义url地址
        let url = "../brand/add.do";
        //7.7.2)根据id是否存在，修改url地址
        if($scope.entity.id){
            url = "../brand/update.do";
        }
        //7.7.3)执行操作
        brandService.save(url,$scope.entity).success(response=>{
            if(response.success){
                //刷新页面
                $scope.search();
            }else{
                alert(response.message);
            }
        })
    }
    //7.9)根据数组下标删除品牌
    $scope.del=()=>{
        brandService.del($scope.selectIds).success(response=>{
            if(response.success){
                //刷新页面
                $scope.search();
            }else{
                alert(response.message);
            }
        })
    }
})
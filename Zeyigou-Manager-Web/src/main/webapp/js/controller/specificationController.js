app.controller("specificationController",function($scope,$controller,specificationService){
    $controller('baseController',{$scope:$scope});//继承

    //0.定义规格复合对象（包含规格及规格对应的选项列表）
    $scope.entity={spec:{},specificationOptionList:[]}

    //1.查询品牌列表
    $scope.findAll=()=>{
        specificationService.findAll().success(response=>{
            $scope.list = response;
        })
    }
    //2.添加规格选项
    $scope.addSpec=()=>{
        $scope.entity.specificationOptionList.push({});
    }
    //3.保存规格
    $scope.save=()=>{
        //3.1)定义url
        let url = "../specification/add.do";
        //3.2)如果entity.spec.id存在，则执行修改
        if($scope.entity.spec.id){
            url = "../specification/update.do";
        }
        //3.3)调用save方法，进行保存
        specificationService.save(url,$scope.entity).success(resp=>{
            if(resp.success){
                $scope.findAll();
            }else{
                alert(resp.message);
            }
        })
    }
    //4. 根据规格id查询规格对象
    $scope.findOne=(id)=>{
        specificationService.findOne(id).success(resp=>{
            $scope.entity = resp;
        })
    }

    //5.删除规格
    $scope.dele=()=>{
        specificationService.dele($scope.selectIds).success(resp=>{
            if(resp.success){
                $scope.findAll();
            }else{
                alert(resp.message);
            }
        })
    }
})
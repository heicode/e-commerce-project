app.controller("typeTemplateController",function($scope,$controller,typeTemplateService,specificationService,brandService){
    $controller('baseController',{$scope:$scope});//继承

    //1.查询所有的模板
    $scope.findAll=()=>{
        typeTemplateService.findAll().success(response=>{
            $scope.list = response;
        })
    }
    //2.查询品牌列表
    $scope.selectBrandList=()=>{
        brandService.selectBrandList().success(response=>{
            $scope.brandList = {data:response};
        })
    }
    //3.查询规格列表
    $scope.findSpecList=()=>{
        specificationService.findSpecList().success(response=>{
            $scope.specList = {data:response};
        })
    }
    //4.保存
    $scope.save=()=>{
        let url = "../typeTemplate/add.do";
        if($scope.entity.id){
            url = "../typeTemplate/update.do";
        }
        typeTemplateService.save(url,$scope.entity).success(resp=>{
            if(resp.success){
                $scope.findAll();
            }else{
                alert(resp.message);
            }
        })
    }
    //5.删除模板
    $scope.dele=()=>{
        typeTemplateService.dele($scope.selectIds).success(resp=>{
            if(resp.success){
                $scope.findAll();
            }else{
                alert(resp.message);
            }
        })
    }
    //6.修改界面操作
    $scope.updateUI=(temp)=>{
        $scope.entity = temp;
        //将品牌、规格、自定义扩展属性都转换为json对象
        $scope.entity.brandIds = JSON.parse($scope.entity.brandIds);
        $scope.entity.specIds = JSON.parse($scope.entity.specIds);
        $scope.entity.customAttributeItems = JSON.parse($scope.entity.customAttributeItems);

    }
    //7.处理json的显示效果，只显示指定json属性的内容
    $scope.showInfo=(list,key)=>{
        //7.1)定义最终显示的字符串
        let info = "";
        //7.2)遍历集合
        //7.2.1)将传入的list参数转换为json对象
        list = JSON.parse(list);
        //7.2.2)遍历list对象
        list.forEach(a=>{
            info += a[key] + ",";
        })
        //7.3)去掉多余的逗号
        return info.substr(0,info.length-1);

    }
})
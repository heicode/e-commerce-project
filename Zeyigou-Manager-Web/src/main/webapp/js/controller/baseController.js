 //品牌控制层 
app.controller('baseController' ,function($scope){	
	

	//分页控件配置
	$scope.paginationConf = {
		currentPage: 1,				//当前页
		totalItems: 10,				//总记录数
		itemsPerPage: 10,			//每页大小
		perPageOptions: [10, 20, 30, 40, 50],  //分页下拉选项
		onChange: function(){
			//$scope.findByPage();//重新加载
			$scope.search();
		}
	};

	$scope.selectIds = [];		//存放要删除的品牌id
	//7.8)定义向要删除的id数组中动态添加或删除数据
	$scope.updateSelection=(event,id)=>{
		if(event.target.checked){   //event.target:代表正在点击的复选框控件
			$scope.selectIds.push(id);
		}else{
			//① 根据id查询此id中数组中的索引下标
			let index = $scope.selectIds.indexOf(id);
			//② 根据索引下标从数组删除元素
			$scope.selectIds.splice(index,1);		//参数1：代表要删除的元素在数组中的下标，参数2：代表删除个数
		}
	}



	
});	
app.controller("sellerController",function($scope,$controller,sellerService){
    $controller('baseController',{$scope:$scope});//继承

    //1.查询所有商家
    $scope.findAll=()=>{
        sellerService.findAll().success(resp=>{
            $scope.list = resp;
        })
    }
    //2.显示详情
    $scope.showDetail=(seller)=>{
        $scope.entity = seller;
    }
    //3.商家审核
    $scope.updateStatus=(status)=>{
        sellerService.updateStatus($scope.entity.sellerId,status).success(resp=>{
            if(resp.success){
                $scope.findAll();
            }else{
                alert(resp.message);
            }
        })
    }
})
app.service("brandService",function($http){
    //1.查询所有品牌
    this.findAll=()=>{
        return $http.get("../brand/list.do");
    }
    //2.分页查询
    this.findByPage=(page,pageSize)=>{
        return $http.get("../brand/findByPage.do?page="+page+"&pageSize="+pageSize);

    }
    //3.分页带条件查询
    this.search=(page,pageSize,entity)=>{
        return $http.post("../brand/search.do?page="+page+"&pageSize="+pageSize,entity);
        console.log(entity);
    }
    //4.保存品牌
    this.save=(url,entity)=>{
        return $http.post(url,entity);
    }
    //5.删除品牌
    this.del=(ids)=>{
        return $http.get("../brand/delete.do?ids="+ids);
    }
    //6.查询所有品牌列表
    this.selectBrandList=()=>{
        return $http.get("../brand/selectBrandList.do");
    }
})
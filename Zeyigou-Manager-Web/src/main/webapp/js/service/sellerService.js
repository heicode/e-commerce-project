app.service("sellerService",function($http){

    //1.查询所有商家
    this.findAll=()=>{
        return $http.get("../seller/list.do");
    }
    //2.商家审核
    this.updateStatus=(id,status)=>{
        return $http.get("../seller/updateStatus.do?id="+id+"&status="+status);
    }
})
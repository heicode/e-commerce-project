app.service("specificationService",function($http){

    //1.查询规格列表
    this.findAll=()=>{
        return $http.get("../specification/list.do");
    }
    //2.保存规格
    this.save=(url,entity)=>{
        return $http.post(url,entity);
    }
    //3.根据规格id查询规格对象
    this.findOne=(id)=>{
        return $http.get("../specification/findOne.do?id="+id);
    }
    //4.删除规格
    this.dele=(ids)=>{
        return $http.get("../specification/delete.do?ids="+ids);
    }

    //5.查询规格列表（specName-->text）
    this.findSpecList=()=>{
        return $http.get("../specification/findSpecList.do");
    }
})


package com.zeyigou.manager.controller;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import java.util.HashMap;
import java.util.Map;

@RestController
public class LoginController {

    @RequestMapping("login")
    public Map login() {
        Map map = new HashMap<>();
        //1.得到登录名
        String name = SecurityContextHolder.getContext().getAuthentication().getName();
        System.out.println("登录名 " + name);
        //2.将登录名放到map中
        map.put("name", name);
        return map;
    }
}

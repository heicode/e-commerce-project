package com.zeyigou.manager.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.zeyigou.pojo.Result;
import com.zeyigou.pojo.TbTypeTemplate;
import com.zeyigou.sellergoods.service.TypeTemplateService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 模板管理
 */
@RestController
@RequestMapping("typeTemplate")
public class TypeTemplateController {
    @Reference
    private TypeTemplateService templateService;
    //1.查询模板列表
    @RequestMapping("list")
    public List<TbTypeTemplate> findAll(){
        return  templateService.findAll();
    }
    //2.添加模板
    @RequestMapping("add")
    public Result add(@RequestBody TbTypeTemplate template){
        templateService.add(template);
        return  new Result(true,"添加成功！");
    }
    //3.修改模板
    @RequestMapping("update")
    public Result update(@RequestBody TbTypeTemplate typeTemplate){
        templateService.update(typeTemplate);
        return  new Result(true,"修改模板成功！");
    }
    //4.删除模板
    @RequestMapping("delete")
    public Result delete(Long[] ids){
        templateService.delete(ids);
        return new Result(true,"删除模板！");
    }
}

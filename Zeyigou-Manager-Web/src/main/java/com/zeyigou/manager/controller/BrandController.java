package com.zeyigou.manager.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.zeyigou.pojo.PageResult;
import com.zeyigou.pojo.Result;
import com.zeyigou.pojo.TbBrand;
import com.zeyigou.sellergoods.service.BrandService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("brand")
public class BrandController {
    @Reference
    private BrandService brandService;
    @RequestMapping("list")
    //1.查询所有品牌
    public List<TbBrand> findAll() {


        return brandService.findAll();
    }
    //2.品牌列表分页
    @RequestMapping("findByPage")
    public PageResult<TbBrand> findByPage(@RequestParam(required = true,defaultValue = "1")int page, int pageSize){

    return  brandService.findByPage(page,pageSize);

    }
    //3.带条件的分页查询
    @RequestMapping("search")
    public PageResult<TbBrand> search(int page , int pageSize, @RequestBody(required = false)TbBrand brand){
        System.out.println(" 分页 " );
    return  brandService.search(page,pageSize,brand);
    }
    //4.添加品牌
    @RequestMapping("add")
    public Result add(@RequestBody TbBrand brand){

        try {
            brandService.add(brand);
            return new Result(true,"添加品牌成功！");
        } catch (Exception e) {
            e.printStackTrace();
            return  new Result(false,"添加品牌失败！");
        }
    }
    //5.修改品牌
    @RequestMapping("update")
    public Result update(@RequestBody TbBrand brand){
        try {
            brandService.update(brand);
            return new Result(true,"修改品牌成功！");
        } catch (Exception e) {
            e.printStackTrace();
            return  new Result(false,"修改品牌失败！");
        }

    }
    //6.删除品牌
    @RequestMapping("delete")
    public  Result delete(String[] ids){

        try {
            brandService.delete(ids);
            return new Result(true,"删除成功！");
        } catch (Exception e) {
            e.printStackTrace();
            return  new Result(false,"删除失败！");
        }
    }
    @RequestMapping("selectBrandList")
    public  List<Map>  selectBrandList(){
List<Map> maps=  brandService.selcetBrandList();
        System.out.println("maps = " + maps);
        return maps;
    }


}

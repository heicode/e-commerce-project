package com.zeyigou.manager.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.zeyigou.group.Specification;
import com.zeyigou.pojo.Result;
import com.zeyigou.pojo.TbSpecification;
import com.zeyigou.sellergoods.service.SpecificationService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * 规格管理控制器
 */
@RestController
@RequestMapping("specification")
public class SpecificationController {
    @Reference
    private SpecificationService specificationService;

    @RequestMapping("list")
    public List<TbSpecification> findAll() {

        return specificationService.findAll();

    }

    //2.添加规格
    @RequestMapping("add")
    public Result add(@RequestBody Specification specification) {
        try {
            specificationService.add(specification);
            return new Result(true,"添加成功！");
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,"添加失败！");

        }

    }
    //3.修改规格
    @RequestMapping("update")
    public  Result update(@RequestBody Specification specification){

        try {
            specificationService.update(specification);
            return  new Result(true,"添加成功！");
        } catch (Exception e) {
            e.printStackTrace();
            return  new Result(false,"添加失败！");

        }
    }
//4.根据规格id查询规格对象
    @RequestMapping("findOne")
    public Specification specification(Long id){
        return specificationService.findOne(id);
    }
    //5.删除规格
    @RequestMapping("delete")
    public Result delete(Long[] ids){
specificationService.delete(ids);
return  new Result(true,"删除成功！");
    }

    @RequestMapping("findSpecList")
    public List<Map> findSpecList(){
        return specificationService.findSpecList();
    }

}

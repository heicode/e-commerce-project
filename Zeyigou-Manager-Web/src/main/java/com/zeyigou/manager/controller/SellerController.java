package com.zeyigou.manager.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.zeyigou.pojo.Result;
import com.zeyigou.pojo.TbSeller;
import com.zeyigou.sellergoods.service.SellerService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("seller")
public class SellerController {
    @Reference
    private SellerService sellerService;

    //1.商家列表
    @RequestMapping("list")
    public List<TbSeller> findAll() {
        return  sellerService.findAll();
    }
    //2.商家审核
    @RequestMapping("updateStatus")
public Result updateStatus(String id ,String status){
        System.out.println("id = " + id+"status"+status);
        try {
            sellerService.updateStatus(id,status);
            return  new Result(true,"审核通过！");
        } catch (Exception e) {
            e.printStackTrace();
            return  new Result(false,"审核未通过！");

        }
        //3.


    }

}

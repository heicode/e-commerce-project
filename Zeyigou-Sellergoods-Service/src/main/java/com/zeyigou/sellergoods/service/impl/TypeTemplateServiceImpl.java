package com.zeyigou.sellergoods.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.zeyigou.manager.TbTypeTemplateMapper;
import com.zeyigou.pojo.TbTypeTemplate;
import com.zeyigou.sellergoods.service.TypeTemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Service
public class TypeTemplateServiceImpl implements TypeTemplateService {
    @Autowired
    private TbTypeTemplateMapper templateMapper;

    //1.模板列表
    @Override
    public List<TbTypeTemplate> findAll() {
        return templateMapper.selectAll();
    }

    //2.添加模板
    @Override
    public void add(TbTypeTemplate template) {
        templateMapper.insert(template);
    }

    //3。修改模板
    @Override
    public void update(TbTypeTemplate typeTemplate) {
        templateMapper.updateByPrimaryKey(typeTemplate);
    }

    //4.删除模板
    @Override
    public void delete(Long[] ids) {
        for (Long id : ids) {
            templateMapper.deleteByPrimaryKey(id);
        }
    }
}

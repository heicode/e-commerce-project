package com.zeyigou.sellergoods.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.zeyigou.manager.TbSellerMapper;
import com.zeyigou.pojo.TbSeller;
import com.zeyigou.sellergoods.service.SellerService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Service
public class SellerServiceImpl implements SellerService {
    @Autowired
    private TbSellerMapper sellerMapper;

    //1.商家列表
    @Override
    public List<TbSeller> findAll() {
        return sellerMapper.selectAll();

    }

    //2.商家审核
    @Override
    public void updateStatus(String id, String status) {
        //2.1通过id查找到对应审核的对象
        TbSeller seller = sellerMapper.selectByPrimaryKey(id);
        //2.2)将页面编辑的审核状态修改到对象中
        seller.setStatus(status);
        //2.3)修改数据库中的对象，达到修审核状态的目的
        sellerMapper.updateByPrimaryKey(seller);
    }

    //2.添加商家
    @Override
    public void add(TbSeller seller) {
        sellerMapper.insert(seller);
    }

    @Override
    public TbSeller findOne(String username) {
        return sellerMapper.selectByPrimaryKey(username);
    }

}

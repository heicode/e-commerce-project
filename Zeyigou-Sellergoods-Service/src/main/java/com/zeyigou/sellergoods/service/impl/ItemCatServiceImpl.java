package com.zeyigou.sellergoods.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.zeyigou.manager.TbItemCatMapper;
import com.zeyigou.pojo.TbItemCat;
import com.zeyigou.sellergoods.service.ItemCatService;
import org.springframework.beans.factory.annotation.Autowired;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

@Service
public class ItemCatServiceImpl implements ItemCatService {
    @Autowired
    private TbItemCatMapper itemCatMapper;

    //根据parentId查询子分类
    @Override
    public List<TbItemCat> findByParentId(Long parentId) {

        //2.直接使用tk的实例
        Example example = new Example(TbItemCat.class);
        //3.带条件查询
        Example.Criteria criteria = example.createCriteria();
        //4.添加条件
        criteria.andEqualTo("parentId", parentId);
        //1.查询实例
        return itemCatMapper.selectByExample(example);
    }
}

package com.zeyigou.sellergoods.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.zeyigou.group.Specification;
import com.zeyigou.manager.TbSpecificationMapper;
import com.zeyigou.manager.TbSpecificationOptionMapper;
import com.zeyigou.pojo.TbSpecification;
import com.zeyigou.pojo.TbSpecificationOption;
import com.zeyigou.pojo.TbSpecificationOptionExample;
import com.zeyigou.sellergoods.service.SpecificationService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;

@Service
public class SpecificationServiceImpl implements SpecificationService {
    @Autowired
    private TbSpecificationMapper specificationMapper;
    @Autowired
    private TbSpecificationOptionMapper optionMapper;

    @Override
    public List<TbSpecification> findAll() {
        return specificationMapper.selectAll();
    }

    //2.添加规格
    @Override
    public void add(Specification specification) {
        //2.1）添加规格
        specificationMapper.insert(specification.getSpec());
        //2.2)得到规格id
        Long id = specification.getSpec().getId();

        //2.3）添加规格选项
        //2.3.1）得到规格选项列表
        List<TbSpecificationOption> optionList = specification.getSpecificationOptionList();
        //2.3.2)遍历规格选项，并设置specId的关联关系
        for (TbSpecificationOption option : optionList) {
            option.setSpecId(id);
            //2.3.3)添加规格选项
            optionMapper.insert(option);
        }
    }

    //3.修改规格
    @Override
    public void update(Specification specification) {
        //3.1）修改规格对象
        specificationMapper.updateByPrimaryKey(specification.getSpec());
        //3.2)对于规格选项的处理分为
        //3.2.1)先根据外键specId从规格选项表中删除规格选项
        Long id = specification.getSpec().getId();
        TbSpecificationOptionExample optionExample = new TbSpecificationOptionExample();
        TbSpecificationOptionExample.Criteria criteria = optionExample.createCriteria();
        criteria.andSpecIdEqualTo(id);
        optionMapper.deleteByExample(optionExample);

        //3.3)再向规格选项表中添加新记录
        List<TbSpecificationOption> optionList = specification.getSpecificationOptionList();
        for (TbSpecificationOption option : optionList) {
            //3.3.1）将option与specid绑定
            option.setSpecId(id);
            //3.3.2)添加数据到数据库
            optionMapper.insert(option);
        }
    }
    //4.根据id查询规格组合对象
    @Override
    public Specification findOne(Long id) {
        //4.1）查询规格对象
        TbSpecification specification = specificationMapper.selectByPrimaryKey(id);
        //4.2）查询出规格选项表
        TbSpecificationOptionExample example = new TbSpecificationOptionExample();
        TbSpecificationOptionExample.Criteria criteria = example.createCriteria();
        criteria.andSpecIdEqualTo(id);
        List<TbSpecificationOption> options = optionMapper.selectByExample(example);

        //4.3)定义组合对象
        Specification specification1 = new Specification();
        //4.4)为组合对象设置关联属性
        specification1.setSpec(specification);
        specification1.setSpecificationOptionList(options);
        return specification1;
    }
    //5.删除规格
    @Override
    public void delete(Long[] ids) {
        //5.1）遍历要删除的id
        for (Long id : ids) {
            //5.2）删除规格
            specificationMapper.deleteByPrimaryKey(id);
           //5.3)删除规格选项
            TbSpecificationOptionExample optionExample = new TbSpecificationOptionExample();
            TbSpecificationOptionExample.Criteria criteria = optionExample.createCriteria();
            criteria.andSpecIdEqualTo(id);
            optionMapper.deleteByExample(optionExample);


        }
    }
    //6.规格列表

    @Override
    public List<Map> findSpecList() {
        return specificationMapper.findSpecList();
    }


}

package com.zeyigou.sellergoods.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.zeyigou.manager.TbBrandMapper;
import com.zeyigou.pojo.PageResult;
import com.zeyigou.pojo.TbBrand;
import com.zeyigou.pojo.TbBrandExample;
import com.zeyigou.sellergoods.service.BrandService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;

//这个注解要是alibab的
@Service
public class BrandServiceImpl implements BrandService {
    @Autowired
    private TbBrandMapper tbBrandMapper;

    //1.品牌列表
    @Override
    public List<TbBrand> findAll() {
        return tbBrandMapper.selectAll();
    }

    //2.品牌列表分页
    @Override
    public PageResult<TbBrand> findByPage(int page, int pageSize) {
        //2.1）开始分页
        PageHelper.startPage(page, pageSize);
        //2.2）得到实例,并进行实例查询
        TbBrandExample example = new TbBrandExample();
        List<TbBrand> tbBrands = tbBrandMapper.selectByExample(example);
        //2.3）转换成Page<TbBrand>对象
        Page<TbBrand> brandPage = (Page<TbBrand>) tbBrands;
        //2.4）返回PageResult对象
        return new PageResult<>(brandPage.getTotal(), brandPage.getResult());
    }
    //3.带条件的分页查询
    @Override
    public PageResult<TbBrand> search(int page, int pageSize, TbBrand brand) {
        //3.1）开始分页
        PageHelper.startPage(page,pageSize);
        //3.2)得到实例
        TbBrandExample example = new TbBrandExample();
        //3.3）添加查询条件
        TbBrandExample.Criteria criteria = example.createCriteria();
        //3.4)如果传过来有值
        if (brand!=null){
            //3.5）名字不为空就添加模糊查询条件
            if (StringUtils.isNotBlank(brand.getName())) {
                criteria.andNameLike("%"+brand.getName()+"%");
            }
            //3.6）如果首字母不为空就添加为查询条件
            if (StringUtils.isNotBlank(brand.getFirstChar())){
                criteria.andFirstCharEqualTo(brand.getFirstChar());
            }
        }
        //3.7）进行实例查询
        List<TbBrand> tbBrands = tbBrandMapper.selectByExample(example);
        //3.8）将实例转化成Page<TbBrand>对象
        Page<TbBrand> brandPage = (Page<TbBrand>) tbBrands;
        //3.9)返回PageResult对象
        return  new PageResult<>(brandPage.getTotal(),brandPage.getResult());
    }
//4添加品牌
    @Override
    public void add(TbBrand brand) {
        tbBrandMapper.insert(brand);
    }
    //5.修改品牌
    @Override
    public void update(TbBrand brand) {
       tbBrandMapper.updateByPrimaryKey(brand);
    }

    @Override
    public void delete(String[] ids) {
        for (String id : ids) {
            tbBrandMapper.deleteByPrimaryKey(id);
        }
    }
    //品牌 列表集合

    @Override
    public List<Map> selcetBrandList() {
      return tbBrandMapper.selectBrandList();

    }
}


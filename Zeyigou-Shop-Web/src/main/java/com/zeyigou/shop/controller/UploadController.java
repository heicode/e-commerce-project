package com.zeyigou.shop.controller;

import com.zeyigou.pojo.Result;
import com.zy.util.FastDFSClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * 文件上传
 */
@RestController
@RequestMapping
public class UploadController {
    @Value("${FILE_SERVER_URL}")
    private String FILE_SERVER_URL;

    //上传文件
    @RequestMapping("upload")
    public Result upload(MultipartFile file) {
        try {
            //1.得到文件名
            String filename = file.getOriginalFilename();
            System.out.println("filename = " + filename);
            //2.构造的工具类
            FastDFSClient fastDFSClient = new FastDFSClient("classpath:Config/fdfs_client.conf");
            //3.得到文件内容
            byte[] fileContent = file.getBytes();
            //4.得到文件后缀名
            String suffixName = filename.substring(filename.lastIndexOf(".") + 1);
            //5.处理文件路径
            String path = fastDFSClient.uploadFile(fileContent, suffixName);
            path=FILE_SERVER_URL+path;
            System.out.println("path = " + path);
            //6.返回上传成功！(将路径当信息传给页面显示图片)
            return  new Result(true,path);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,"上传失败！");
        }


    }
}

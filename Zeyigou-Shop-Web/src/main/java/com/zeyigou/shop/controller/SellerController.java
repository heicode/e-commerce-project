package com.zeyigou.shop.controller;

import com.alibaba.dubbo.config.annotation.Reference;

import com.zeyigou.pojo.Result;
import com.zeyigou.pojo.TbSeller;
import com.zeyigou.sellergoods.service.SellerService;
import org.aspectj.weaver.bcel.BcelAccessForInlineMunger;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
@RequestMapping("seller")
public class SellerController {
@Reference
    private SellerService sellerService;
//1.注册用户
@RequestMapping("add")
public Result add(@RequestBody TbSeller seller){
    //1.1)密码加密
    BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
    String password = encoder.encode(seller.getPassword());
seller.setPassword(password);
seller.setStatus("0");
seller.setCreateTime(new Date());
sellerService.add(seller);
return new Result();
}
//2.
}

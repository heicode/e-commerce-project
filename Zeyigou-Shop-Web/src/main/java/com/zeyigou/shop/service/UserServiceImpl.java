package com.zeyigou.shop.service;

import com.zeyigou.pojo.TbSeller;
import com.zeyigou.sellergoods.service.SellerService;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.ArrayList;
import java.util.List;

public class UserServiceImpl implements UserDetailsService {
    private SellerService sellerService;
    public void  setSellerService(SellerService sellerService){
        this.sellerService=sellerService;
    }
    //1.比对用户输入密码和数据库的对比
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        //2.根据用户名得到用户对象
     TbSeller seller= sellerService.findOne(username);
       //3.如果用户存在就开始认证
        if (seller!=null){
            if (seller.getStatus().equals("1")){
                //4.定义角色集合
                List<SimpleGrantedAuthority> authorities = new ArrayList<>();
          authorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
          return  new User(username,seller.getPassword(),authorities);
            }
        }
        return null;
    }
}

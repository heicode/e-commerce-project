app.controller("itemCatController",function($scope,$controller,itemCatService){
    $controller('baseController',{$scope:$scope});//继承

    //1.根据父分类查询出此分类下的子分类列表
    $scope.findByParentId=(pid)=>{
        itemCatService.findByParentId(pid).success(resp=>{
            $scope.list = resp;
        })
    }
    //2.定义分类的级别变量
    $scope.grade = 1;
    //3.定义修改级别的方法
    $scope.setGrade=(grade)=>{
        $scope.grade = grade;
    }
    //4.显示分类级别
    $scope.showLevel=(itemCat)=>{
        //4.1)根据分类的级别为三个分类对象赋值
        switch ($scope.grade) {
            case 1:
                $scope.entity_1 = itemCat;
                $scope.entity_2 = null;
                $scope.entity_3 = null;
                break;
            case 2:
                $scope.entity_2 = itemCat;
                $scope.entity_3 = null;
                break;
            case 3:
                $scope.entity_3 = itemCat;
                break;
        }
        //4.2)分所分类id进行子分类列表的查询
        $scope.findByParentId(itemCat.id);
    }
})
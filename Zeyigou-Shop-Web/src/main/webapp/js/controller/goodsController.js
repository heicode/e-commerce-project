app.controller("goodsController",function($scope,$controller,goodsService){
    $controller('baseController',{$scope:$scope});//继承

    //1.定义组合对象
    $scope.entity={
        goods:{},
        goodsDesc:{itemImages:[]},
        items:[]
    }

    //2.保存
    $scope.save=()=>{
        //2.1)从kindeditor中取得内容并为$scope.entity.goodsDesc.introduction这个属性赋值
        $scope.entity.goodsDesc.introduction = editor.html()
        console.log($scope.entity)
    }

    //3.文件上传
    $scope.uploadFile=()=>{
        goodsService.uploadFile().success(resp=>{
            //3.1)如果上传成功，就为$scope.imageEntity.url赋值
            if(resp.success){
                $scope.imageEntity.url = resp.message;
            }else{
                alert(resp.message);
            }
        })
    }
})
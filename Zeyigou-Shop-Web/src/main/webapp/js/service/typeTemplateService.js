app.service("typeTemplateService",function($http){

    //1.查询模板列表
    this.findAll=()=>{
        return $http.get("../typeTemplate/list.do");
    }

    //2.保存模板
    this.save=(url,entity)=>{
        return $http.post(url,entity);
    }
    //3.删除模板
    this.dele=(ids)=>{
        return $http.get("../typeTemplate/delete.do?ids="+ids);
    }
})


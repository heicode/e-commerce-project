package com.zeyigou.sellergoods.service;

import com.zeyigou.pojo.TbSeller;

import java.util.List;

public interface SellerService {
    List<TbSeller> findAll();

    void updateStatus(String id, String status);

    void add(TbSeller seller);

    TbSeller findOne(String username);
}

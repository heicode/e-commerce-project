package com.zeyigou.sellergoods.service;

import com.zeyigou.pojo.TbTypeTemplate;

import java.util.List;

public interface TypeTemplateService {
    List<TbTypeTemplate> findAll();

    void add(TbTypeTemplate template);

    void update(TbTypeTemplate typeTemplate);

    void delete(Long[] ids);
}

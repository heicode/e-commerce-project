package com.zeyigou.sellergoods.service;

import com.zeyigou.group.Specification;
import com.zeyigou.pojo.TbSpecification;

import java.util.List;
import java.util.Map;

public interface SpecificationService {
    List<TbSpecification> findAll();

    void add(Specification specification);

    void update(Specification specification);

    Specification findOne(Long id);

    void delete(Long[] ids);

    List<Map> findSpecList();


}

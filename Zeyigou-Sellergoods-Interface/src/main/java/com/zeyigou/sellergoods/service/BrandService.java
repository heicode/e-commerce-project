package com.zeyigou.sellergoods.service;

import com.zeyigou.pojo.PageResult;
import com.zeyigou.pojo.TbBrand;

import java.util.List;
import java.util.Map;

public interface BrandService {
    List<TbBrand> findAll();

    PageResult<TbBrand> findByPage(int page, int pageSize);

    PageResult<TbBrand> search(int page, int pageSize, TbBrand tbBrand);

    void add(TbBrand brand);

    void update(TbBrand brand);

    void delete(String[] ids);

    List<Map> selcetBrandList();

}

package com.zeyigou.sellergoods.service;

import com.zeyigou.pojo.TbItemCat;

import java.util.List;

public interface ItemCatService {
    List<TbItemCat> findByParentId(Long parentId);
}

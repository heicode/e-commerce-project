package com.zeyigou.manager;


import com.zeyigou.pojo.TbProvinces;
import tk.mybatis.mapper.common.Mapper;

public interface TbProvincesMapper extends Mapper<TbProvinces> {

}
package com.zeyigou.manager;


import com.zeyigou.pojo.TbOrderItem;
import tk.mybatis.mapper.common.Mapper;

public interface TbOrderItemMapper extends Mapper<TbOrderItem> {

}
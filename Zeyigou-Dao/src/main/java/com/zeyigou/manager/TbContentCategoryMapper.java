package com.zeyigou.manager;


import com.zeyigou.pojo.TbContentCategory;
import tk.mybatis.mapper.common.Mapper;

public interface TbContentCategoryMapper extends Mapper<TbContentCategory> {

}
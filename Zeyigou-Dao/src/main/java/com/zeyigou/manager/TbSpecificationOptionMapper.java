package com.zeyigou.manager;



import com.zeyigou.pojo.TbSpecificationOption;
import tk.mybatis.mapper.common.Mapper;

public interface TbSpecificationOptionMapper extends Mapper<TbSpecificationOption> {

}
package com.zeyigou.manager;


import com.zeyigou.pojo.TbFreightTemplate;
import tk.mybatis.mapper.common.Mapper;

public interface TbFreightTemplateMapper extends Mapper<TbFreightTemplate> {

}
package com.zeyigou.test;

import org.csource.fastdfs.ClientGlobal;
import org.csource.fastdfs.StorageClient;
import org.csource.fastdfs.TrackerClient;
import org.csource.fastdfs.TrackerServer;

public class FastDFStest {

    public static void main(String[] args) throws Exception {

        //1.初始化trackerServer
        ClientGlobal.init("D:\\item-zeyigou\\fastDFSdemo\\src\\main\\resources\\fdfs_client.conf");
        //2.定义TrackerClient
        TrackerClient trackerClient = new TrackerClient();
        //3.通过客户端连接得到trackerServer
        TrackerServer trackerServer = trackerClient.getConnection();
        //4.定义storageClient
        StorageClient storageClient = new StorageClient(trackerServer,null);
    //5.进行文件上传
        String[] jpgs = storageClient.upload_appender_file("F:\\练习\\图库\\1.jpg", "jpg", null);
        //6.遍历数组
        for (String jpg : jpgs) {
            System.out.println("jpg = " + jpg);
        }
        
    }
}
